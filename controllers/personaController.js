const { request } = require("express");
const { leerInfo, guardarInfo } = require("../models/guardarInfo");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';
//en el db mandar una copia de la lista
const personas = new Personas(); //lista


const tareasDB = leerInfo()
if(tareasDB){
  //establecer las tareas
  personas.cargarTareasFormArray(tareasDB);
  // console.log('DDDB', tareasDB, 'DBBBd');
  personas._listado = tareasDB
  // console.log('22', personas._listado, '2222');
}

const personaGet = (req, res = response) => {

  const lista = leerInfo();
  // console.log(lista, 'hahah');
  res.json({
    msg: 'post API - Controldor',
    lista,
  });
};

const personaPut =  (req = request, res = response) => {
  const  {id} = req.params;
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const index = personas._listado.findIndex(object => object.id === id)

  if (index !== -1){
    personas.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: 'person editada - Controlador',
    })
  } else {
    res.json ({
      msg: 'person no encontrada',
    })
  }

};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona( nombres, apellidos, ci, direccion, sexo )
  console.log('3333', persona, '333');

//crear el metodo crearpersona
  personas.crearPersona(persona)
  console.log('000',personas._listado, '000');
  guardarInfo(personas._listado)
  const lista = personas._listado
  
  res.json({
    msg: 'persona Creada',
    // lista,
  });

  // personas._listado.push(lista)
  
};

const personaDelete = (req, res = response) => {
  // const  {id} = req.params;
  
  // const index = personas._listado.findIndex(object => object.id === id) 

  // if (index !== -1){
  //   personas.eliminarPersona(index)
  //   guardarInfo(personas._listado)
  //   res.json({
  //     msg: 'person eliminada - Controlador',
  //   })
  // } else {
  //   res.json ({
  //     msg: 'person no encontrada',
  //   })
  // }


  const { id } = req.params
  // console.log('idd', id, 'idd');
  // console.log('eliminar');
  if (id) {
    personas.eliminarPersona(id);
    guardarInfo(personas.personasArr)

    res.json({
    msg: 'person eliminada - Controlador',
    })
    // console.log(leerDB());
  }

};

module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete,
}
